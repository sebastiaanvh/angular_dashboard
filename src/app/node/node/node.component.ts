import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.css'],
})
/**
 * Represents a node component
 */
export class NodeComponent {
  @Input()
  public node!: Node;
}
export interface NodeJson {
  nodeId: number;
  title : string;
  contentType: string;
  username: string;
  originalLanguage: string;
  published: boolean;
}
/**
 * Represents a node
 */
export class Node {
  /**
   *
   * @param {number} _nodeId
   * @param {string} _title
   * @param {string} _contentType
   * @param {string} _username
   * @param {string} _originalLanguage
   * @param {boolean} _published
   */
  constructor(private _nodeId:number, private _title: string,
              private _contentType: string, private _username: string,
              private _originalLanguage: string, private _published: boolean) {
  }

  /**
   *
   * @param {NodeJson} json
   * @return {Node}
   */
  static fromJSON(json:NodeJson) : Node {
    return new Node(json.nodeId, json.title, json.contentType,
        json.username, json.originalLanguage, json.published);
  }

  /**
   * @return {number}
   */
  get nodeId(): number {
    return this._nodeId;
  }

  /**
   * @return {string}
   */
  get title(): string {
    return this._title;
  }

  /**
   * @return {string}
   */
  get contentType(): string {
    return this._contentType;
  }

  /**
   * @return {string}
   */
  get username(): string {
    return this._username;
  }

  /**
   * @return {string}
   */
  get originalLanguage(): string {
    return this._originalLanguage;
  }

  /**
   * @return {boolean}
   */
  get published(): boolean {
    return this._published;
  }
}


