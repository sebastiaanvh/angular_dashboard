
import {Node} from './node/node.component';

const JsonNodes = [
  {
    nodeId: 2,
    title: 'Page not found',
    contentType: 'blog',
    username: 'admin',
    originalLanguage: 'en',
    published: true,
  },
  {
    nodeId: 1,
    title: 'Access denied',
    contentType: 'page',
    username: 'admin',
    originalLanguage: 'en',
    published: false,

  },
  {
    nodeId: 3,
    title: 'About us',
    contentType: 'page',
    username: 'admin',
    originalLanguage: 'en',
    published: true,
  },
  {
    nodeId: 5,
    title: 'About him',
    contentType: 'blog',
    username: 'admin',
    originalLanguage: 'en',
    published: true,
  },

];
export const listofallnodes: Node[] = JsonNodes.map(Node.fromJSON);

