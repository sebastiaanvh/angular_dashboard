import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NodeComponent} from './node/node.component';
import {NodesListComponent} from './nodes-list/nodes-list.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
@NgModule({
  imports: [
    CommonModule, HttpClientModule, FormsModule,
  ],
  exports: [NodesListComponent],
  declarations: [NodeComponent, NodesListComponent,],
})
/**
 * Represent a Node module
 */
export class NodeModule { }


