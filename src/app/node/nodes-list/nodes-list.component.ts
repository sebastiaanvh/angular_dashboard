import {Component, Input} from '@angular/core';
import {DashboardDataService} from '../../dashboard-data.service';
import {Node} from '../node/node.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-nodes-list',
  templateUrl: './nodes-list.component.html',
  styleUrls: ['./nodes-list.component.css'],
})

/**
 * Represents a list of nodes
 */
export class NodesListComponent {
  @Input() user: string;
  private _fetchNodes$: Observable<Node[]>  = this._dashboardDataService.nodes$;

  /**
   *
   * @param {DashboardDataService} _dashboardDataService
   */
  constructor(private _dashboardDataService: DashboardDataService) {
  }

  /**
   * @return {Observable<Node[]>}
   */

  get nodes$(): Observable<Node[]> {
    return this._fetchNodes$;
  }

}
