import {Component, ElementRef} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
/**
 * Represents a App
 */
export class AppComponent {
  title = 'dashboardapp';
  user:string;

  /**
   *
   */
  constructor(private elementRef:ElementRef) {
      this.user = this.elementRef.nativeElement.getAttribute('user');
    }
  }


