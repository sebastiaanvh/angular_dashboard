import {ChartItemModel} from './models/chart-item.model';
import {ChartTypeModel} from './models/chart-type.model';
const jsonChartsPublication = [
  {
    value: 33,
    name: 'published',
  },
  {
    value: 1,
    name: 'unpublished',
  },

];

const jsonChartsTranslation = [
  {
    value: 33,
    name: 'EN',
  },
  {
    value: 12,
    name: 'NL',
  },
  {
    value: 12,
    name: 'DE',
  },


];
const jsonChartsContentTypes = [
  {
    value: 12,
    name: 'blog',
  },
  {
    value: 13,
    name: 'page',
  },
  {
    value: 9,
    name: 'job',
  },
];
const jsonChartsPublicationsContentTypes = [
  {
    value: 6,
    name: 'published blogs',
  },
  {
    value: 6,
    name: 'unpublished blogs',
  },
  {
    value: 7,
    name: 'published pages',
  },
  {
    value: 3,
    name: 'unpublished pages',
  },
  {
    value: 9,
    name: 'published jobs',
  },
  {
    value: 0,
    name: 'unpublished jobs',
  },
];
const jsonTypes = [
  {
    type: 'publications',
    pieChartItem: jsonChartsPublication.map(ChartItemModel.fromJSON),
  },
  {
    type: 'translations',
    pieChartItem: jsonChartsTranslation.map(ChartItemModel.fromJSON),
  },
  {
    type: 'content_types',
    pieChartItem: jsonChartsContentTypes.map(ChartItemModel.fromJSON),
  },
  {
    type: 'content_type_publications',
    pieChartItem: jsonChartsPublicationsContentTypes
        .map(ChartItemModel.fromJSON),
  },
]
export const pieChartList: ChartTypeModel[] = jsonTypes
    .map(ChartTypeModel.fromJSON);

