import {Component, Input} from '@angular/core';

import {EChartsType} from 'echarts';
import {ChartWrapperComponent} from '../chart-wrapper/chart-wrapper.component';
import {DashboardDataService} from "../../dashboard-data.service";
import {catchError, map} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css'],
})
/**
 * Represents a pie chart
 */
export class PieChartComponent {

  private echartsInstance: EChartsType;

  /**
   *
   * @param _dashboardDataService
   */
  constructor(private _dashboardDataService: DashboardDataService) {
  }
  @Input() user:string;
  @Input() set dataType(dataType:string) {
    try {
      this.echartsInstance
    }catch(e){
      console.log("wait on init")
    }
    this.givePieChart(dataType,this.user).subscribe((dataList) => {
      try {
        this.echartsInstance.setOption({
          ...this.chartOption,
          legend: {
            x: 'center',
            y: 'bottom',
            data: dataList.map((x) => {
              return x.name;
            }),
          },
          series:
            {
              name: 'nodes',
              type: 'pie',
              radius: [30, 110],
              data: dataList,
            },
        }, false);
        this.echartsInstance.setOption({
          title: {
            text: dataType,
            x: 'center',
          },
        });
      } catch (error) {

      }
    })
  }

  givePieChart(dataType:string,user:string):Observable<{ value: number, name: string }[]> {
    return this._dashboardDataService.charts$(dataType,user).pipe(map((chartItem) => {
      return chartItem.map(
        (fieldsOfChartItem) => {
          return {
            value: fieldsOfChartItem.value,
            name: fieldsOfChartItem.name,
          };
        },
      );
    },))
  }
  /**
   *
   * @param {EChartsType} ec
   */
  onChartInit(ec: EChartsType) {
    this.echartsInstance = ec;
  }

  public chartOption = {
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)',
    },
    calculable: true,
    series: [{
      type: 'pie',
    }],
  };
}
