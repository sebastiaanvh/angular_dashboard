import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-chart-wrapper',
  templateUrl: './chart-wrapper.component.html',
  styleUrls: ['./chart-wrapper.component.scss'],
})
/**
 * Class representing a Wrapper for charts
 */
export class ChartWrapperComponent {
  public selectedTypes:Array<string> = [];
  @Input() user:string;
  constructor() {
  }
  /**
   *
   * @param {any} type
   */
  onSubmit(type: any) {
   this.selectedTypes = type.value;
  }

}
