import {Component, Input} from '@angular/core';
import {EChartsType} from 'echarts';
import {ChartWrapperComponent} from '../chart-wrapper/chart-wrapper.component';
import {DashboardDataService} from "../../dashboard-data.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css'],
})
/**
 * Class representing a bar chart
 */
export class BarChartComponent {

  private echartsInstance: EChartsType;

  /**
   *
   * @param _dashboardDataService
   */
  constructor(private _dashboardDataService: DashboardDataService) {
  }
  /**
   *
   * @param {EChartsType} ec
   */
  onChartInit(ec: EChartsType) {
    console.log("bar chart init");
    this.echartsInstance = ec;
  }
  @Input() user:string;
  @Input() set dataType(dataType:string){
  try{
    this.echartsInstance
  }catch (e) {
    console.log("wacht op init")
  }
    this.giveBarChart(dataType,this.user).subscribe((dataList) => {
        try {
          this.echartsInstance.setOption({title: {
              text: dataType,
              x: 'center',
            }, });
          this.echartsInstance.setOption({
            ...this.options,
            xAxis: [
              {
                type: 'category',
                data: dataList.map((x) => {
                  return x.name;
                }),
              },
            ],
            yAxis: [{
              type: 'value',
            }],
            series: [{
              name: 'Counters',
              type: 'bar',
              barWidth: '40%',
              data: dataList.map((x) => {
                return x.value;
              }),
            }],
          });
        } catch (error) {
        }
      },
    );


  }
  giveBarChart(dataType:string,user:string):Observable<{ value: number, name: string }[]> {
    return this._dashboardDataService.charts$(dataType,user).pipe(map((chartItem) => {
      return chartItem.map(
        (fieldsOfChartItem) => {
          return {
            value: fieldsOfChartItem.value,
            name: fieldsOfChartItem.name,
          };
        },
      );
    },))
  }

  initOpts = {
    title: {
      text: 'publications',
      x: 'center',
    },
    renderer: 'svg',
    width: 600,
    height: 300,
  };

  options = {
    color: ['#3398DB'],
    xAxis: [{
      type: 'category',
    }],
    yAxis: [{
      type: 'value',
    }],
    series: [{
      type: 'bar',
      showBackground: true,
      backgroundStyle: {
        color: 'rgba(180, 180, 180, 0.2)',
      },
    }],

  };


}
