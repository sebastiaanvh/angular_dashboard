export interface ChartItem {
  value:number,
  name:string
}

/**
 * Represents a ChartItem
 */
export class ChartItemModel {
  /**
   *
   * @param {number} _value
   * @param {string} _name
   */
  constructor(private _value: number, private _name: string) {
  }
  /**
   *
   * @param {ChartItem} json
   * @return {ChartItemModel}
   */
  static fromJSON(json: ChartItem): ChartItemModel {
    return new ChartItemModel(json.value, json.name);
  }

  /**
   * @return {number} value
   */
  get value(): number {
    return this._value;
  }

  /**
   *
   * @param {number} value
   */
  set value(value: number) {
    this._value = value;
  }

  /**
   * @return {string} name
   */
  get name(): string {
    return this._name;
  }

  /**
   *
   * @param {number} value
   */
  set name(value: string) {
    this._name = value;
  }
}
