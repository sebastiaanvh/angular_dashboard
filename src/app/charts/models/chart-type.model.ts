import {ChartItemModel} from './chart-item.model';

export interface ChartTypeJson {
  type:string,
  pieChartItem:ChartItemModel[];
}

/**
 * Represents a chart type model
 */
export class ChartTypeModel {
  /**
   *
   * @param {string} _type
   * @param {ChartItemModel[]} _pieChartItem
   */
  constructor(private _type: string, private _pieChartItem: ChartItemModel[]) {
  }

  /**
   * @return {ChartTypeModel}
   * @param {ChartTypeJson} json
   */
  static fromJSON(json: ChartTypeJson): ChartTypeModel {
    return new ChartTypeModel(json.type, json.pieChartItem);
  }

  /**
   * @return {string}
   */
  get type(): string {
    return this._type;
  }

  /**
   *
   * @param {string} value
   */
  set type(value: string) {
    this._type = value;
  }

  /**
   * @return {ChartItemModel}
   */
  get pieChartItem(): ChartItemModel[] {
    return this._pieChartItem;
  }
}
