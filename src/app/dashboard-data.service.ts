import {Injectable} from '@angular/core';
import {listofallnodes} from './node/mock-nodes';
import {Node} from './node/node/node.component';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {map, catchError, tap, shareReplay} from 'rxjs/operators';
import {pieChartList} from './charts/mock-charts';
import {ChartTypeModel} from './charts/models/chart-type.model';
import {ChartItemModel} from './charts/models/chart-item.model';

@Injectable({
  providedIn: 'root',
})
/**
 * Represents dashboard data service
 */
export class DashboardDataService {
  private _nodes: Node[] = listofallnodes;
  private _observableList: BehaviorSubject<Node[]>
    = new BehaviorSubject<Node[]>(listofallnodes);
  private _pieChartsList: BehaviorSubject<ChartTypeModel[]>
    = new BehaviorSubject<ChartTypeModel[]>(pieChartList);

  /**
   *
   * @param {HttpClient} http
   */
  constructor(private http: HttpClient) {
  }

  /**
   * @return {Observable<Node[]>}
   */
  get nodes$(): Observable<Node[]> {
    return this.http.get(`https://projecten-3-groep-7-webshop-aoste.dd:8443/en/cegeka_dashboard/test_resource/nodes`).pipe(
        catchError(this.handleError),
        tap(console.log),
        shareReplay(1),
        map((value) => value[0]),
        map((list: any[]): Node[] => list.map(Node.fromJSON)),
    );
  }
  userNodes$(user:string): Observable<Node[]> {
    return this.http.get(`https://projecten-3-groep-7-webshop-aoste.dd:8443/en/cegeka_dashboard/test_resource/nodes?user=` + user).pipe(
      catchError(this.handleError),
      tap(console.log),
      shareReplay(1),
      map((value) => value[0]),
      map((list: any[]): Node[] => list.map(Node.fromJSON)),
    );
  }

  /**
   *
   * @param {string} type
   * @param user
   * @return {Observable<ChartItemModel[]>}
   */
  charts$(type:string,user:string): Observable<ChartItemModel[]> {
    return this.http.get(`https://projecten-3-groep-7-webshop-aoste.dd:8443/en/cegeka_dashboard/test_resource/` + type + `?user=` + user).pipe(
        tap(console.log),
        shareReplay(1),
        map((list) => list.map(ChartItemModel.fromJSON)),
        catchError(this.handleError),

    );
  }

  /**
   *
   * @param  {any} err
   * @return {Observable<never>}
   */
  handleError(err: any): Observable<never> {
    let errorMessage: string;
    if (err instanceof HttpErrorResponse) {
      errorMessage =
        `'${err.status} ${err.statusText}' when accessing '${err.url}'`;
    } else {
      errorMessage = `an unknown error occurred ${err}`;
    }
    console.error(err);
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  /**
   * @return {Observable<Node[]>}
   */
  get observableList(): Observable<Node[]> {
    return this._observableList.asObservable();
  }

  /**
   * @return {Observable<ChartTypeModel[]>}
   */
  get pieChartList(): Observable<ChartTypeModel[]> {
    return this._pieChartsList.asObservable();
  }
}
