import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NodeModule} from './node/node.module';
import {PieChartComponent} from './charts/pie-chart/pie-chart.component';
import {NodesListComponent} from './node/nodes-list/nodes-list.component';
import {NgxEchartsModule} from 'ngx-echarts';
import {ChartWrapperComponent}
  from './charts/chart-wrapper/chart-wrapper.component';
import {BarChartComponent} from './charts/bar-chart/bar-chart.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";


@NgModule({
  declarations:
    [
      AppComponent, PieChartComponent, ChartWrapperComponent, BarChartComponent,
    ], imports:
    [
      BrowserModule, AppRoutingModule, NodeModule,
      NgxEchartsModule.forRoot({
        echarts: () =>
          import('echarts'),
        // or import('./path-to-my-custom-echarts')
      }), FormsModule, BrowserAnimationsModule, MatSelectModule, MatInputModule, ReactiveFormsModule,
    ],
  providers:
    [
      NodesListComponent, ChartWrapperComponent,
    ],
  exports: [],
  bootstrap: [AppComponent],
})
/**
 * Represents an app
 */
export class AppModule {
}
